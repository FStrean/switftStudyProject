// Создаем массив количества дней в месяцах
let daysInMonths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

// Создаем массив названий месяцев
let monthNames = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]

// Выводим количество дней в каждом месяце без названий
for days in daysInMonths {
    print(days)
}

// Выводим название месяца и количество дней в нем
for (index, days) in daysInMonths.enumerated() {
    print("\(monthNames[index]) - \(days) дней")
}

// Создаем массив tuples с параметрами (имя месяца, кол-во дней)
var monthDays:Array<(String,Int)> = []
for (index, days) in daysInMonths.enumerated() {
    monthDays.append((monthNames[index], days))
}

// Выводим название месяца и количество дней в нем из массива tuples
for (month, days) in monthDays {
    print("\(month) - \(days) дней")
}

// Выводим количество дней в каждом месяце в обратном порядке без названий
for days in daysInMonths.reversed() {
    print(days)
}

// Произвольно выбранная дата (месяц и день)
let month = 5 // Июнь
let day = 15 // 15-е число

// Считаем количество дней до этой даты от начала года
let daysUntilDate = daysInMonths[0..<month-1].reduce(0, +) + day
print("Количество дней до выбранной даты: \(daysUntilDate)")

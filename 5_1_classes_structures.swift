class Vehicle {
    var brand: String
    
    init(brand: String) {
        self.brand = brand
    }
    
    func drive() {
        print("\(brand) is driving")
    }
}

class Car: Vehicle {
    var numberOfWheels: Int
    
    init(brand: String, numberOfWheels: Int) {
        self.numberOfWheels = numberOfWheels
        super.init(brand: brand)
    }
    
    override func drive() {
        print("\(brand) car with \(numberOfWheels) wheels is driving")
    }
}

class Motorcycle: Vehicle {
    var engineSize: Int
    
    init(brand: String, engineSize: Int) {
        self.engineSize = engineSize
        super.init(brand: brand)
    }
    
    override func drive() {
        print("\(brand) motorcycle with \(engineSize)cc engine is driving")
    }
}

let motorcycle = Motorcycle(brand: "Toyota", engineSize: 2)
motorcycle.drive()

let car = Car(brand: "BMW", numberOfWheels: 4)
car.drive()

class House {
    var width: Double
    var height: Double
    var isDestroyed: Bool = false
    
    init(width: Double, height: Double) {
        self.width = width
        self.height = height
    }
    
    func create() {
        let area = width * height
        print("The house is created with an area of \(area) square meters")
    }
    
    func destroy() {
        isDestroyed = true
        print("The house is destroyed")
    }
}

let house = House(width: 25, height: 30)
house.create()
house.destroy()


class Student: CustomStringConvertible {
    var name: String
    var age: Int
    var grade: Int
    
    init(name: String, age: Int, grade: Int) {
        self.name = name
        self.age = age
        self.grade = grade
    }
    
    var description: String {
        name + " " + String(age) + " years old " + String(grade) + " grade"
    }
}

class School {
    var students: [Student]
    
    init(students: [Student]) {
        self.students = students
    }
    
    func sortByName() {
        students.sort { $0.name < $1.name }
    }
    
    func sortByAge() {
        students.sort { $0.age < $1.age }
    }
    
    func sortByGrade() {
        students.sort { $0.grade > $1.grade }
    }
}


let school = School(students: [Student(name: "Bob", age: 14, grade: 5), 
Student(name: "Kate", age: 11, grade: 3), 
Student(name: "Mike", age: 13, grade: 5), 
Student(name: "Andrew", age: 16, grade: 2), 
Student(name: "Michael", age: 12, grade: 4), 
Student(name: "Andy", age: 12, grade: 4), 
Student(name: "Bob", age: 11, grade: 4)])
print(school.students)
school.sortByName()
print(school.students)
school.sortByAge()
print(school.students)
school.sortByGrade()
print(school.students)


struct Point {
    var x: Int
    var y: Int
    
    mutating func moveBy(dx: Int, dy: Int) {
        x += dx
        y += dy
    }
}

class Person {
    var name: String
    var age: Int
    
    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }
    
    func sayHello() {
        print("Hello, my name is \(name)")
    }
}

var point = Point(x: 0, y: 0)
point.moveBy(dx: -5, dy: 1)
print(String(point.x) + ", " + String(point.y))

let person = Person(name: "Daniel", age: 22)
person.sayHello()

// Основное различие между структурами и классами заключается в том, что структуры - это значения типа, в то время как классы - это ссылочные типы.

// Другие различия:
// Структуры передаются по значению, а классы передаются по ссылке.
// Структуры не могут наследоваться, а классы могут.
// Деинициализаторы позволяют экземпляру класса освободить любые ресурсы, которые он использовал, в структурах такого нет
// Приведение типов позволяет проверить и интерпретировать тип экземпляра класса в процессе выполнения

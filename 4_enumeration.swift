enum Animal: String {
    case cat = "Кошка"
    case dog = "Собака"
}

enum Fruit: Int {
    case apple = 1
    case banana = 2
}

enum Gender {
    case male
    case female
}

enum AgeCategory {
    case young
    case middleAged
    case old
}

enum Experience {
    case beginner
    case intermediate
    case advanced
}

struct Employee {
    var gender: Gender
    var ageCategory: AgeCategory
    var experience: Experience
}

enum RainbowColor {
    case red
    case orange
    case yellow
    case green
    case blue
    case indigo
    case violet
}

func printRainbowColors() {
    let colors: [RainbowColor] = [.red, .orange, .yellow, .green, .blue, .indigo, .violet]
    
    for color in colors {
        switch color {
        case .red:
            print("apple red")
        case .orange:
            print("orange orange")
        case .yellow:
            print("lemon yellow")
        case .green:
            print("lime green")
        case .blue:
            print("blueberry blue")
        case .indigo:
            print("blueberry indigo")
        case .violet:
            print("grape violet")
        }
    }
}

printRainbowColors()

enum Score: String {
    case A = "Отлично"
    case B = "Хорошо"
    case C = "Удовлетворительно"
    case D = "Неудовлетворительно"
}

func assignGrade(for score: Score) -> Int {
    switch score {
    case .A:
        return 5
    case .B:
        return 4
    case .C:
        return 3
    case .D:
        return 2
    }
}

let myScore = Score.A
let myGrade = assignGrade(for: myScore)
print("Моя оценка: \(myScore.rawValue) - \(myGrade)")


enum CarMake {
    case volkswagen
    case bmw
    case toyota
    case skoda
}

enum CarType {
    case sedan
    case coupe
    case suv
    case truck
}

// Создание структуры для представления автомобиля
struct Car {
    var make: CarMake
    var type: CarType
    var year : Int
}

// Создание списка автомобилей в гараже
let garage = [
    Car(make: .toyota, type: .sedan, year: 2020),
    Car(make: .volkswagen, type: .coupe, year: 2021),
    Car(make: .bmw, type: .truck, year: 2011),
    Car(make: .skoda, type: .suv, year: 2020),
]

func printCarsInGarage() {
    for car in garage {
        print("\(car.make) \(car.year), type: \(car.type)")
    }
}

printCarsInGarage()

// Создаем метод, который принимает имена друзей и возвращает отсортированный массив
func sortFriendsByLength(names: String...) -> [String] {
    return names.sorted { $0.count < $1.count }
}

func sortFriendsByLength(names: String...) -> [String] {
    return names.sorted { $0.count > $1.count }
}

// Пример использования метода
let sortedFriends = sortFriendsByLength(names: "Alice", "Bob", "Charlie", "Dave", "Eve")
print(sortedFriends) // ["Bob", "Alice", "Dave", "Charlie", "Eve"]

// Создаем словарь, где ключ - кол-во символов в имени, а в значении - имя друга
var friendsDict = [Int: String]()
for name in sortedFriends {
    friendsDict[name.count] = name
}

// Функция, которая принимает ключ и выводит его значение из словаря
func printFriendNameForKey(_ key: Int) {
    if let friendName = friendsDict[key] {
        print("Friend with \(key) letters in their name: \(friendName)")
    } else {
        print("No friend with \(key) letters in their name.")
    }
}

printFriendNameForKey(3)

// Функция, которая проверяет массивы на пустоту и выводит их в консоль
func printNonEmptyArrays(strArray: [String], numArray: [Int]) {
    if strArray.isEmpty {
        print("The string array is empty.")
        strArray.append("Default Value")
    }
    if numArray.isEmpty {
        print("The number array is empty.")
        numArray.append(0)
    }
    print("String array: \(strArray)")
    print("Number array: \(numArray)")
}

var emptyStrArray = [String]()
var emptyNumArray = [Int]()
printNonEmptyArrays(strArray: emptyStrArray, numArray: emptyNumArray)

// Протокол *Hotel* с инициализатором
protocol Hotel {
    init(roomCount: Int)
}

class HotelAlfa: Hotel {
    var roomCount: Int
    
    required init(roomCount: Int) {
        self.roomCount = roomCount
    }
}

// Протокол GameDice у него {get} свойство numberDice
protocol GameDice {
    var numberDice: Int { get }
}

extension Int: GameDice {
    var numberDice: Int {
        print("Выпало \(self) на кубике")
        return self
    }
}

let diceCoub = 4
let b = diceCoub.numberDice

protocol SomeProtocol {
    var propertyOne: String { get set }
    var propertyTwo: Int? { get }
    
    func someMethod()
}


class SomeClass: SomeProtocol {
    var propertyOne: String = "Property One"
    var propertyTwo: Int?
    
    func someMethod() {
        print("Вызван метод someMethod")
    }
}


// Компания, пишущая код
enum Platform {
    case iOS, Android, Web
}

protocol Coding {
    var time: Int { get set }
    var codeCount: Int { get set }
    func writeCode(platform: Platform, numberOfSpecialist: Int)
}

protocol Stoppable {
    func stopCoding()
}

class Company: Coding, Stoppable {
    var programmersCount: Int
    var specializations: [Platform]
    var time: Int = 0
    var codeCount: Int = 0
    
    init(programmersCount: Int, specializations: [Platform]) {
        self.programmersCount = programmersCount
        self.specializations = specializations
    }
    
    func writeCode(platform: Platform, numberOfSpecialist: Int) {
        guard programmersCount >= numberOfSpecialist else {
            print("Недостаточно программистов для написания кода")
            return
        }
        guard specializations.contains(platform) else {
            print("Компания не работает на этой платформе")
            return
        }
        print("Разработка началась. Пишем код для \(platform)")
        time += 1
        codeCount += numberOfSpecialist * 100
    }
    
    func stopCoding() {
        print("Работа закончена. Сдаю в тестирование")
    }
}

let company = Company(programmersCount: 10, specializations: [.iOS, .Web])

company.writeCode(platform: .iOS, numberOfSpecialist: 5)
company.writeCode(platform: .Android, numberOfSpecialist: 3)
company.writeCode(platform: .iOS, numberOfSpecialist: 8)

company.stopCoding()

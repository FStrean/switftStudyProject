struct IOSCollection<Element>: Collection {
    private var storage: Storage
    
    subscript(index: Int) -> Element {
        get {
            return storage.elements[index]
        }
        set {
            copyIfNeeded()
            storage.elements[index] = newValue
        }
    }
    
    mutating func append(_ newElement: Element) {
        copyIfNeeded()
        storage.elements.append(newElement)
    }
    
    private mutating func copyIfNeeded() {
        if !isKnownUniquelyReferenced(&storage) {
            storage = Storage(elements: storage.elements)
        }
    }
    
    private class Storage {
        var elements: [Element]
        
        init(elements: [Element]) {
            self.elements = elements
        }
    }
}

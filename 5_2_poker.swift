enum Suit: String, CaseIterable {
    case spades = "Пиковый"
    case hearts = "Червовый"
    case diamonds = "Бубновый"
    case clubs = "Трефовый"
}

enum Rank: Int, CaseIterable {
    case two = 2, three, four, five, six, seven, eight, nine, ten
    case jack, queen, king, ace
    
    var stringValue: String {
        switch self {
        case .jack:
            return "J"
        case .queen:
            return "Q"
        case .king:
            return "K"
        case .ace:
            return "A"
        default:
            return "\(rawValue)"
        }
    }
}

struct Card {
    let rank: Rank
    let suit: Suit
    
    var description: String {
        return "\(rank.stringValue)\(suit.rawValue)"
    }
}


func checkHand(_ hand: [Card]) -> String {
    guard hand.count == 5 else {
        return "Invalid hand"
    }
    
    let sortedHand = hand.sorted { $0.rank.rawValue < $1.rank.rawValue }
    
    let isFlush = Set(sortedHand.map { $0.suit }).count == 1
    if isFlush {
        return hand.first!.suit.rawValue + " флеш"
    }
    
    let isStraight = sortedHand.last!.rank.rawValue - sortedHand.first!.rank.rawValue == 4 && Set(sortedHand.map { $0.rank }).count == 5
    if isStraight {
        return "Стрит"
    }
    
    let isStraightFlush = isFlush && isStraight
    if isStraightFlush {
        return hand.first!.suit.rawValue + " стрит флеш"
    }
    
    let isRoyalFlush = isFlush && sortedHand.first!.rank == .ten && sortedHand.last!.rank == .ace
    if isRoyalFlush {
        return hand.first!.suit.rawValue + " флеш рояль"
    } 
    
    let ranks = sortedHand.map { $0.rank }
    let rankCounts = Dictionary(grouping: ranks, by: { $0 }).mapValues { $0.count }
    let maxCount = rankCounts.values.max()!
    
    switch maxCount {
    case 4:
        return  hand.first!.suit.rawValue + " каре"
    case 3:
        if rankCounts.values.contains(2) {
            return "Фулл хаус"
        } else {
            return "Тройка"
        }
    case 2:
        if rankCounts.values.contains(3) {
            return "Фулл хаус"
        } else if rankCounts.values.contains(2) {
            return "Две пары"
        } else {
            return "Пара"
        }
    default:
        return "Старшая карта"
    }
}

var deck = [Card]()

for suit in Suit.allCases  {
    for rank in Rank.allCases {
        deck.append(Card(rank: rank, suit: suit))
    }
}

// Функция для выбора n случайных карт из колоды
func selectCards(_ n: Int) -> [Card] {
    var selectedCards = [Card]()
    
    for _ in 1...n {
        let randomIndex = Int.random(in: 0..<deck.count)
        selectedCards.append(deck[randomIndex])
        deck.remove(at: randomIndex)
    }
    
    return selectedCards
}

print(checkHand(selectCards(5)))
